/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

/**
 *
 * @author ASUS
 */
public class DOCGIA {
    private String MaDG;
    private String TenDG;
    private String SDT;
    private String Email;

    public DOCGIA() {
    }

    public DOCGIA(String MaDG, String TenDG, String SDT, String Email) {
        this.MaDG = MaDG;
        this.TenDG = TenDG;
        this.SDT = SDT;
        this.Email = Email;
    }

    public String getMaDG() {
        return MaDG;
    }

    public void setMaDG(String MaDG) {
        this.MaDG = MaDG;
    }

    public String getTenDG() {
        return TenDG;
    }

    public void setTenDG(String TenDG) {
        this.TenDG = TenDG;
    }

    public String getSDT() {
        return SDT;
    }

    public void setSDT(String SDT) {
        this.SDT = SDT;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }
    
}
