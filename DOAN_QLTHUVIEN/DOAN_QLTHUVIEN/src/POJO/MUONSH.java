/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

import java.util.Date;

/**
 *
 * @author ASUS
 */
public class MUONSH {
    private String MaSach;
    private String MaDG;
    private String MaNV;
    private String Ngaymuon;
    private String NgayTra;
    private String TinhTrang;

    public MUONSH() {
    }

    public MUONSH(String MaSach, String MaDG, String MaNV, String Ngaymuon, String NgayTra, String TinhTrang) {
        this.MaSach = MaSach;
        this.MaDG = MaDG;
        this.MaNV = MaNV;
        this.Ngaymuon = Ngaymuon;
        this.NgayTra = NgayTra;
        this.TinhTrang = TinhTrang;
    }

    public String getMaSach() {
        return MaSach;
    }

    public void setMaSach(String MaSach) {
        this.MaSach = MaSach;
    }

    public String getMaDG() {
        return MaDG;
    }

    public void setMaDG(String MaDG) {
        this.MaDG = MaDG;
    }

    public String getMaNV() {
        return MaNV;
    }

    public void setMaNV(String MaNV) {
        this.MaNV = MaNV;
    }

    public String getNgaymuon() {
        return Ngaymuon;
    }

    public void setNgaymuon(String Ngaymuon) {
        this.Ngaymuon = Ngaymuon;
    }

    public String getNgayTra() {
        return NgayTra;
    }

    public void setNgayTra(String NgayTra) {
        this.NgayTra = NgayTra;
    }

    public String getTinhTrang() {
        return TinhTrang;
    }

    public void setTinhTrang(String TinhTrang) {
        this.TinhTrang = TinhTrang;
    }
    
}
