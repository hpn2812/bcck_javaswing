/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.DOCGIA;
import POJO.SACH;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class DocGiaDao {
    public static ArrayList<DOCGIA> layDSdg(String sql){
        ArrayList<DOCGIA> DSdocgia = new ArrayList<>();
        try {
            ConnectDB cn= new ConnectDB();
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {                
                DOCGIA dg = new DOCGIA();
                dg.setMaDG(rs.getString(1));
                dg.setTenDG(rs.getString(2));
                dg.setSDT(rs.getString(3));
                dg.setEmail(rs.getString(4));
                DSdocgia.add(dg);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return DSdocgia;
    }
    public static boolean themDG(DOCGIA sa,String sql) throws Exception
   {
       boolean kq=false;
        ConnectDB cn= new ConnectDB();
      
       int n=cn.executeUpdate(sql);
       if(n==1)
       {
           kq=true;
       }
       cn.Close();
       return kq;
   }
    
     public static boolean suadg(String tendg,String sdt,String email,String madg) {
        boolean kq = false;
        String sql = "update DOCGIA set TenDG=N'" + tendg + "',SDT='"+sdt+"',Email='"+email+"' where MaDG='"+madg+"'";
       ConnectDB cn= new ConnectDB();
        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        try {
            cn.Close();
        } catch (Exception ex) {
            Logger.getLogger(DocGiaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return kq;
    }
     
      public static boolean xoadg(String madg) throws Exception
   {
       boolean kq=false;
       String sql="delete from DOCGIA where MaDG='"+madg+"'";
       ConnectDB cn= new ConnectDB();
    
       int n=cn.executeUpdate(sql);
       if(n==1)
       {
           kq=true;
       }
       cn.Close();
       return kq;
   }
}
