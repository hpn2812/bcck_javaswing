/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.NHANVIEN;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class NhanVienDao {
      public static ArrayList<NHANVIEN> getListEmployee(){
        ArrayList<NHANVIEN> list = new ArrayList<>();
        try {
            String sql = "select * from nhanvien ";
           ConnectDB cn= new ConnectDB();
           
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {                
                NHANVIEN nv  = new NHANVIEN();
                nv.setMaNV(rs.getString(1));
                nv.setTenNV(rs.getString(2));
                nv.setNgayTruc(rs.getDate(3).toLocalDate());
                list.add(nv);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
      
      
       
    public static boolean addEmployee(NHANVIEN em, String sql) throws Exception {
        boolean kq = false;
        ConnectDB cn = new ConnectDB();
        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        cn.Close();
        return kq;
    }
    
     public static boolean deleteNV(String manv) throws Exception {
        boolean kq = false;
        String sql = "delete from nhanvien where manv= '" + manv + "'";
        ConnectDB cn = new ConnectDB();
        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        cn.Close();
        return kq;
    }

    public static boolean updateEmployee(String tennv, LocalDate ngTruc, String manv) {
        boolean kq = false;
        String sql = "update nhanvien set tennv=N'" + tennv + "',ngaytruc='"+ngTruc+ "' where manv='" + manv + "'";
        ConnectDB cn = new ConnectDB();
        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        try {
            cn.Close();
        } catch (Exception ex) {
            Logger.getLogger(NhanVienDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return kq;
    }
    
    
     public static ArrayList<NHANVIEN> Search(String sql){
        ArrayList<NHANVIEN> list = new ArrayList<>();
        try {
            ConnectDB cn= new ConnectDB();
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {                
             NHANVIEN em = new NHANVIEN();
                em.setMaNV(rs.getString(1));
                em.setTenNV(rs.getString(2));
                em.setNgayTruc(rs.getDate(3).toLocalDate());   
                list.add(em);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
}
