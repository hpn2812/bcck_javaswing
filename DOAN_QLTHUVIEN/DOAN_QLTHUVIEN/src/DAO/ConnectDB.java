/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author huynh
 */
public class ConnectDB {

    private Connection con;

    public ConnectDB() {
        String url = "net.sourceforge.jtds.jdbc.Driver";
        try {
            Class.forName(url);
            String dbUrl = "jdbc:jtds:sqlserver://PHONGNHA:1433/DA_QuanLyThuVien";
            try {
                con = DriverManager.getConnection(dbUrl);
            } catch (SQLException ex) {
                Logger.getLogger(ConnectDB.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConnectDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ResultSet executeQuery(String sql)
    {
        ResultSet rs = null;
        try {
            Statement sm = con.createStatement();
            rs = sm.executeQuery(sql);
        } catch (Exception e) {
           
        }
        return rs;
    }
     public int executeUpdate(String sql) {
        int rs = -1;
        try {
            Statement sm = con.createStatement();
            rs = sm.executeUpdate(sql);
        } catch (Exception e) {

        }
        return rs;
    }
    public void Close() throws Exception {
        con.close();
    }

}
