package DAO;

import POJO.SACH;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JLabel;
import GUI.QL_Sach;

/**
 *
 * @author ASUS
 */
public class SachDao {

    public static ArrayList<SACH> layDSSach(String sql) {
        ArrayList<SACH> DSsach = new ArrayList<>();
        try {
            ConnectDB cn = new ConnectDB();

            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {
                SACH sach = new SACH();
                sach.setMaSach(rs.getString(1));
                sach.setTenSH(rs.getString(2));
                sach.setNXB(rs.getString(3));
                sach.setNAMXB(rs.getInt(4));
                sach.setTacGia(rs.getString(5));
                sach.setTinhTrang(rs.getString(6));
                sach.setSoluong(rs.getInt(7));
                DSsach.add(sach);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return DSsach;
    }

    public static boolean suasach(int sl, String ma) throws Exception {
        boolean kq = false;
        String sql = "update SACH set SOLUONG=" + sl + "where MASH='" + ma + "'";
        ConnectDB cn = new ConnectDB();

        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        cn.Close();
        return kq;
    }

    public static boolean themsh(SACH sa, String sql) throws Exception {
        boolean kq = false;
        ConnectDB cn = new ConnectDB();

        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        cn.Close();
        return kq;
    }
    
      public static boolean xoash(String mash) throws Exception
    {
        boolean kq=false;
       String sql="delete from SACH WHERE MASH='"+mash+"'";
      ConnectDB cn= new ConnectDB();
       
       int n=cn.executeUpdate(sql);
       if(n==1)
       {
           kq=true;
       }
       cn.Close();
       return kq;
    }
}
