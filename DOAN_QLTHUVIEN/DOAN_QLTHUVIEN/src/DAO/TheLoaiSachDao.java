
package DAO;

import POJO.LOAISH;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class TheLoaiSachDao {
    public static ArrayList<LOAISH> layDSLoaiSach(){
        ArrayList<LOAISH> DSloai = new ArrayList<>();
        try {
            String sql = "select TenLoai from LOAISH ";
           ConnectDB cn= new ConnectDB();
           
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {                
                LOAISH loai  = new LOAISH();
                loai.setTenLoai(rs.getString(1));
                DSloai.add(loai);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return DSloai;
    }
    
    
}
