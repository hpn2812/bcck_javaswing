/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.MUONSH;
import POJO.SACH;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class MuonSachDao {

    public static ArrayList<MUONSH> laydsmuon(String sql) {
        ArrayList<MUONSH> DSmuon = new ArrayList<>();
        try {
            ConnectDB cn = new ConnectDB();

            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {
                MUONSH s = new MUONSH();
                s.setMaSach(rs.getString(1));
                s.setMaDG(rs.getString(2));
                s.setMaNV(rs.getString(3));
                s.setNgaymuon(rs.getString(4));
                s.setNgayTra(rs.getString(5));
                s.setTinhTrang(rs.getString(6));
                DSmuon.add(s);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return DSmuon;
    }

    public static boolean themMS(MUONSH sa) throws Exception {
        boolean kq = false;
        String sql = "insert into MUONSH(MaSH,MaDG,MaNV,NgayMuon,NgayTra) values('" + sa.getMaSach() + "',N'" + sa.getMaDG() + "','" + sa.getMaNV() + "','"
                + sa.getNgaymuon() + "','" + sa.getNgayTra() + "')";
        ConnectDB cn = new ConnectDB();

        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        cn.Close();
        return kq;
    }

    public static boolean suamuonsh(String tt, String madg) throws Exception {
        boolean kq = false;
        String sql = "delete from DOCGIA set TinhTrang=" + tt + "where MaDG='" + madg + "'";
        ConnectDB cn = new ConnectDB();

        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        cn.Close();
        return kq;
    }

    public static boolean xoamuon(String madg) throws Exception {
        boolean kq = false;
        String sql = "delete from MUONSH WHERE MaDG='" + madg + "'";
        ConnectDB cn = new ConnectDB();

        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        cn.Close();
        return kq;
    }
}
