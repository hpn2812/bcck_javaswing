/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

import java.util.Date;

/**
 *
 * @author ASUS
 */
public class NHANVIEN {
    private String MaNV;
    private String TenNV;
    private Date NgayTruc;

    public NHANVIEN() {
    }

    public NHANVIEN(String MaNV, String TenNV, Date NgayTruc) {
        this.MaNV = MaNV;
        this.TenNV = TenNV;
        this.NgayTruc = NgayTruc;
    }

    public String getMaNV() {
        return MaNV;
    }

    public void setMaNV(String MaNV) {
        this.MaNV = MaNV;
    }

    public String getTenNV() {
        return TenNV;
    }

    public void setTenNV(String TenNV) {
        this.TenNV = TenNV;
    }

    public Date getNgayTruc() {
        return NgayTruc;
    }

    public void setNgayTruc(Date NgayTruc) {
        this.NgayTruc = NgayTruc;
    }
    
}
