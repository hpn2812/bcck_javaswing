/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

/**
 *
 * @author ASUS
 */
public class SACH {
    private String MaSach;
    private String TenSH;
    private int NAMXB;
    private String NXB;
    private String TacGia;
    private String TinhTrang;
    private String MaLoai;
    private int Soluong;

    public SACH() {
    }

    public SACH(String MaSach, String TenSH, int NAMXB, String NXB, String TacGia, String TinhTrang, String MaLoai, int Soluong) {
        this.MaSach = MaSach;
        this.TenSH = TenSH;
        this.NAMXB = NAMXB;
        this.NXB = NXB;
        this.TacGia = TacGia;
        this.TinhTrang = TinhTrang;
        this.MaLoai = MaLoai;
        this.Soluong = Soluong;
    }

    public int getSoluong() {
        return Soluong;
    }

    public void setSoluong(int Soluong) {
        this.Soluong = Soluong;
    }
    
    public String getNXB() {
        return NXB;
    }

    public void setNXB(String NXB) {
        this.NXB = NXB;
    }
    
    public String getMaSach() {
        return MaSach;
    }

    public void setMaSach(String MaSach) {
        this.MaSach = MaSach;
    }

    public String getTenSH() {
        return TenSH;
    }

    public void setTenSH(String TenSH) {
        this.TenSH = TenSH;
    }

    public int getNAMXB() {
        return NAMXB;
    }

    public void setNAMXB(int NAMXB) {
        this.NAMXB = NAMXB;
    }

    public String getTacGia() {
        return TacGia;
    }

    public void setTacGia(String TacGia) {
        this.TacGia = TacGia;
    }

    public String getTinhTrang() {
        return TinhTrang;
    }

    public void setTinhTrang(String TinhTrang) {
        this.TinhTrang = TinhTrang;
    }

    public String getMaLoai() {
        return MaLoai;
    }

    public void setMaLoai(String MaLoai) {
        this.MaLoai = MaLoai;
    }
    
}
