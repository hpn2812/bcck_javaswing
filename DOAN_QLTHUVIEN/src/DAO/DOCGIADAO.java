/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.DOCGIA;
import POJO.SACH;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class DOCGIADAO {
    public static ArrayList<DOCGIA> layDSdg(String sql){
        ArrayList<DOCGIA> DSdocgia = new ArrayList<>();
        try {
            ConnectDB cn= new ConnectDB();
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {                
                DOCGIA dg = new DOCGIA();
                dg.setMaDG(rs.getString(1));
                dg.setTenDG(rs.getString(2));
                dg.setSDT(rs.getString(3));
                dg.setEmail(rs.getString(4));
                DSdocgia.add(dg);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return DSdocgia;
    }
}
