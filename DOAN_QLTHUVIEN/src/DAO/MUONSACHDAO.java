/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.MUONSH;
import POJO.SACH;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class MUONSACHDAO {
     public static ArrayList<MUONSH> laydsmuon(String sql){
        ArrayList<MUONSH> DSmuon = new ArrayList<>();
        try {
            ConnectDB cn= new ConnectDB();
       
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {                
                MUONSH s=new MUONSH();
                s.setMaSach(rs.getString(1));
                s.setMaDG(rs.getString(2));
                s.setMaNV(rs.getString(3));
                s.setNgaymuon(rs.getString(4));
                s.setNgayTra(rs.getString(5));
                s.setTinhTrang(rs.getString(6));
                DSmuon.add(s);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return DSmuon;
    }
}
