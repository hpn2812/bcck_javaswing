
package DAO;

import POJO.SACH;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JLabel;
import GUI.QUANLI_SH;

/**
 *
 * @author ASUS
 */
public class SACHDAO {
    public static ArrayList<SACH> layDSSach(String sql){
        ArrayList<SACH> DSsach = new ArrayList<>();
        try {
           ConnectDB cn= new ConnectDB();
        
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {                
                SACH sach  = new SACH();
                sach.setMaSach(rs.getString(1));
                sach.setTenSH(rs.getString(2));
                sach.setNXB(rs.getString(3));
                sach.setNAMXB(rs.getInt(4));
                sach.setTacGia(rs.getString(5));
                sach.setTinhTrang(rs.getString(6));
                sach.setSoluong(rs.getInt(7));
                DSsach.add(sach);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return DSsach;
    }
}
